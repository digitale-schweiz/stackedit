# Edelmetalle

## Bücher

 - [ ] [**Der Staat im dritten Jahrtausend**](https://www.schulthess.com/buchshop/detail/ISBN-9783727213038/Lichtenstein-II/Der-Staat-im-dritten-Jahrtausend) - *Hans-Adam II. von Liechtenstein*: «Es wäre ein schöner Erfolg, wenn es der Menschheit im dritten Jahrtausend gelingt, alle Staaten in Dienstleistungsunternehmen zu verwandeln, die den Menschen auf der Basis der direkten und indirekten Demokratie sowie des Selbstbestimmungsrechtes auf Gemeindeebene dienen.»

## Händler

 - [Geiger-Edelmetalle](https://www.geiger-edelmetalle.ch/)

## Prüfgeräte
 - [Goldanalytix - Gold Tester - Echtes Gold erkennen - Gold Reinheit feststellen | Goldanalytix.de-Online-Shop (gold-analytix.de)](https://www.gold-analytix.de/shop)
 - [Vertriebspartner werden | Goldanalytix.de-Online-Shop (gold-analytix.de)](https://www.gold-analytix.de/vertriebspartner-werden)
 - 

![enter image description here](https://golddepot-de.auvesta.com/gfx/100g-Silber-von-Heraeus-bei-Auvesta.png)
![enter image description here](https://golddepot-de.auvesta.com/gfx/100g-Gold-von-Heraeus-bei-Auvesta.png)

![enter image description here](https://golddepot-de.auvesta.com/gfx/100g-Platinum-von-Heraeus-bei-Auvesta.png)
![enter image description here](https://golddepot-de.auvesta.com/gfx/100g-Palladium-von-Heraeus-bei-Auvesta.png)

## Investieren

 - [URANIUM RESOURCES FUND](https://www.uraniumresourcesfund.li/)
 - [Incrementum](https://www.incrementum.li/)

## Info

 - [Feinunze und Feingewicht – Gewichtseinheiten bei Edelmetallen (goldpreis.de)](https://www.goldpreis.de/gewichtseinheiten-bei-edelmetallen/)
 - [Sicher vor Fälschungen! Echte Münzen und Barren erkennen (goldpreis.de)](https://www.goldpreis.de/sicherheitsmerkmale-von-muenzen-und-barren/)
 - [Gold prüfen, Echtheitsprüfung und Fälschungen (goldpreis.de)](https://www.goldpreis.de/gold-pruefen/)
 - [Warum investieren? Gold Eigenschaften (goldpreis.de)](https://www.goldpreis.de/warum-in-gold-investieren/)
 - [In Silber investieren, das gibt es zu beachten (goldpreis.de)](https://www.goldpreis.de/warum-in-silber-investieren/)
 - [Platin - Dichte, Vorkommen und Verwendung (goldpreis.de)](https://www.goldpreis.de/platin/)
 - [Ruthenium - Rutheniumbarren Preise vergleichen (gold.de)](https://www.gold.de/kaufen/ruthenium/)
 - [Iridium - Iridiumbarren Preise vergleichen (gold.de)](https://www.gold.de/kaufen/iridium/)
 - [Rhodiumbarren - Preise vergleichen bei GOLD.DE](https://www.gold.de/kaufen/rhodium/rhodiumbarren/)
 - [Rhodiumpulver – Preise auf GOLD.DE](https://www.gold.de/kaufen/rhodium/rhodiumpulver/)
 - [Goldreporter-Shop](https://onlineshop.goldreporter.de/)
 - [Diamant und Brillant 0,094 Carat mit Zertifikat IGI425090039 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/diamant-und-brillant-0-094-carat-mit-zertifikat-igi425090039-19661)
 - [SR-20s | Goldreporter-Shop](https://onlineshop.goldreporter.de/buchpaket-goldlagerung/)
 - [Sachwert Magazin ePaper - Sachwert Magazin (sachwert-magazin.de)](https://sachwert-magazin.de/sachwert-magazin-epaper/)
 - [Zubehör | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/katalog/zubehoer)
 - [Edelmetallzubehör](https://shop.gvs-bullion.com/edelmetallzubehoer.html)

## Gold Münzen

 - [Kruegerrand 100x 1/10 Unze Schatzkiste | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/kruegerrand-100x-110-unze-schatzkiste-21219)
 - [USA - Bitcoin - 1 Oz Kupfer im Online-Shop www.Silbertresor.de kaufen 7235](http://www.silbertresor.de/product_info.php?info=p7712_USA---Bitcoin---1-Oz-Kupfer.html&refID=19)
 - [20x 1/10 Unze MIX Maple Nugget Krügerrand Philharmoniker | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/20x-110-unze-mix-maple-nugget-kruegerrand-philharmoniker-21049)

___

 1. [10 Franken Schweizer Vreneli | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/10-franken-schweizer-vreneli-19801)
 2. [20 Franken Schweizer Vreneli | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/20-franken-schweizer-vreneli-19800)
 3. [100 Franken Schweizer Vreneli | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/100-franken-schweizer-vreneli-21051)
 4. [10 Franken Liechtenstein Goldmünze Fürst Franz I 1930 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/10-franken-liechtenstein-goldmuenze-fuerst-franz-i-1930-21268)
 5. [25 Franken Liechtenstein Goldmünze Prince Franz Josef II 1956 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/25-franken-liechtenstein-goldmuenze-prince-franz-josef-ii-1956-21266)
 6. [25 Franken Liechtenstein Goldmünze Prince Franz Josef II 1961 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/25-franken-liechtenstein-goldmuenze-prince-franz-josef-ii-1961-21267)
 7. [50 Franken Liechtenstein Goldmünze Fürst Franz Joesef II 1961 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/50-franken-liechtenstein-goldmuenze-fuerst-franz-joesef-ii-1961-21269)

### 1/10 Unze
 1. [Känguru 1/10 Unze 2021)](https://www.heubach-edelmetalle.de/verkauf/kaenguru-110-unze-2021-23815) - Nennwert: 15 AUD
 2. [1/10 Unze Wiener Philharmoniker Gold)](https://www.heubach-edelmetalle.de/verkauf/110-unze-wiener-philharmoniker-gold-19759) - Nennwert: 10 EUR
 3. [Britannia 1/10 Unze | Ab 2013](https://www.heubach-edelmetalle.de/verkauf/britannia-110-unze-19805) - Nennwert: 10 Pfund
 4. [American Eagle 1/10 Unze neues Motiv | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/american-eagle-110-unze-neues-motiv-24286) - 5 USD
 5. [Kruegerrand 1/10 Unze | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/kruegerrand-110-unze-19751)
 6. [China Panda 1/10 Unze 2015 Goldpanda 50 Yuan | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/china-panda-110-unze-2015-goldpanda-50-yuan-21111)
 7. 


### 1 Unze Feingold
 1. [Maple Leaf Gold 1 Unze 2021](https://www.heubach-edelmetalle.de/verkauf/maple-leaf-gold-1-unze-2021-19755) - 50 CAD
 2. [1 Unze Wiener Philharmoniker Gold 2021](https://www.heubach-edelmetalle.de/verkauf/1-unze-wiener-philharmoniker-gold-2021-23743) - Nennwert: 100 EUR
 3. [Britannia 1 Unze | Ab 2013)](https://www.heubach-edelmetalle.de/verkauf/britannia-1-unze-19802) - Nennwert: 100 Pfund
 4. [Kangaroo 1 Unze 2021](https://www.heubach-edelmetalle.de/verkauf/kangaroo-1-unze-2021-23821) - Nennwert: 100 AUD
 3.1. [Australien Emu 1 Unze Gold 2021 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/australien-emu-1-unze-gold-2021-24270) - Nennwert: 100 AUD
 3.2. [Australien Super Pit 1 Unze 2021 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/australien-super-pit-1-unze-2021-24194) - Nennwert: 100 AUD
 3.3. [Australien Schwan 1 Unze Gold 2021 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/australien-schwan-1-unze-gold-2021-24278) - Nennwert: 100 AUD
 5. [China Panda 2017 Goldpanda 500 Yuan | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/china-panda-2017-goldpanda-500-yuan-21301) - Nennwert: 500 Yuan
 6. 
 7. 

## Silber Münzen

 1. [Maple Leaf 1 Unze 2022 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/maple-leaf-1-unze-2022-24536) - **Nennwert**: 5 Kanadische Dollar
 2. [Britannia 1 Unze 2022 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/britannia-1-unze-2022-24564) - **Nennwert**: 2 Pfund
 2.1 [Queens Beasts the white Greyhound 2021 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/queens-beasts-white-greyhound-2021-23921) - **Nennwert**: 5 Pfund
 3. [Känguru 1 Unze Silber 2022 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/kaenguru-1-unze-silber-2022-24538) - **Nennwert**: 1 Australischer Dollar
 4. [American Silber Eagle 1 Unze 2022 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/american-silber-eagle-1-unze-2022-24556) - **Nennwert**: 1 Dollar
 5. [Krügerrand Silber 2022 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/kruegerrand-silber-2022-24560) - **Nennwert**: 1 Rand
 6. [China Panda 2021 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/china-panda-2021-23837) - **Nennwert**: 10 CYN
 7. [Philharmoniker 1 Unze 2022 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/philharmoniker-1-unze-2022-24562)- **Nennwert**: 1,5 Euro
 7.1 [10 Euro MIX Silber Gedenkmünzen 925/1000 stempelglanz | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/10-euro-mix-silber-gedenkmuenzen-9251000-stempelglanz-14946)
7.2 [20 Euro Sammlermünzen 2016 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/20-euro-sammlermuenzen-2016-19010)
7.3 [20 Euro Sammlermünzen 2017 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/20-euro-sammlermuenzen-2017-19348)
7.4 [20 Euro Sammlermünzen 2018 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/20-euro-sammlermuenzen-2018-19160)
7.5 [2019 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/katalog/silber/silbermuenzen/euro-muenzen/2019)
8. Somalia - **Nennwert**: 100 Schilling
8.1 [Somalia Leopard 2020 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/somalia-leopard-2020-23929)
8.2. [Somalia Elefant 2021 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/somalia-elefant-2021-23923)
9. Neuseeland - **Nennwert**: 1 Dollar
9.1 [1 Unze Fiji Taku Turtle Schildkröte | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/1-unze-fiji-taku-turtle-schildkroete-13492)
9.2 [Kiwi Neuseeland 2021 | Heubach Edelmetalle (heubach-edelmetalle.de)](https://www.heubach-edelmetalle.de/verkauf/kiwi-neuseeland-2021-23947)

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTQ3OTM0MzEwNiwtMTAyMzYzNDg3Niw3OD
MwOTE5MzcsNzg5MTE3Nzg1LDc4OTExNzc4NSw0MjU5MjQ2MzQs
LTE1MzkzMzQ0NTIsLTMwOTU5MTIyOCwxMzk4ODY1NzkyLDU3Mj
U0MTc3OSwzMDQ3OTgzMzddfQ==
-->